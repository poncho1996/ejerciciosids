var miImage = document.querySelector('img');
var miBoton = document.querySelector('button');
var miTitulo = document.querySelector('h1');

miImage.onclick = function () {
    var miSrc = miImage.getAttribute('src');

    if (miSrc === 'https://i.blogs.es/03839d/firefoxlogo/450_1000.png') {
        miImage.setAttribute('src','http://www.uberbin.net/wp-content/uploads/2009/02/firefox.jpg')
    } else {
        miImage.setAttribute('src', 'https://i.blogs.es/03839d/firefoxlogo/450_1000.png');
    }
}

function estableceNombreUsuario(){
    var miNombre = prompt ('Por favor, Ingresa tu nombre : ');
    localStorage.setItem('nombre', miNombre);
    miTitulo.textContent = 'Mozilla es fresco, ' + miNombre;

   
} 

if (!localStorage.getItem('nombre')){
        estableceNombreUsuario();
    }
else {
    var nombreALmacenado = localStorage.getItem('nombre');
    miTitulo.textContent = 'Mozilla es fresco,' +  nombreALmacenado;
}

miBoton.onclick = function(){
    estableceNombreUsuario();
}